var userbase = exports = module.exports;
var mongoDb = require("mongodb").Db;
var mongoServer = require("mongodb").Server;

var db = {
	open: function() {
		throw "UserBase not configured!"
	}
};

var defaultOK = {
	error: null
};

var userExists = {
	error: "User already exists"
};

var wrongAuth = {
	error: "Wrong uniqueId or password"
};

// Set default values
userbase.host = "localhost";
userbase.db = "test";
userbase.port = 27017;
userbase.prefix = "userbase";

userbase.config = function(config) {
	if (config) {
		userbase.host = config.host || userbase.host;
		userbase.db = config.db || userbase.db;
		userbase.port = config.port || userbase.port;
		userbase.prefix = config.prefix || userbase.prefix;
	}
	db = new mongoDb(userbase.db, new mongoServer(userbase.host, userbase.port), {safe: false});

	if (config && config.reset) {
		userbase.reset();
	}
}

userbase.mainCollection = function() {
	return this.prefix + "_main";
}

userbase.reset = function() {
	db.open(function(err, db) {
		db.collection(userbase.mainCollection()).remove({}, function(err) {
			console.log("Userbase reset");
			db.close();
		});
	});
}

userbase.insertUser = function(req, res) {
	var uniqueId = req.body.uniqueId;
	var user = {
		uniqueId: uniqueId,
		password: req.body.password,
		email: req.body.email,
		meta: req.body.meta,
		ip: req.connection.remoteAddress,
		createdAt: new Date().getTime()
	};

	db.open(function(err, db) {
		var mainColl = db.collection(userbase.mainCollection());
		mainColl.findOne({uniqueId: uniqueId}, function(err, result) {
			if (result) {
				res.send(userExists);
				return db.close();
			}
			mainColl.insert(user, function(err, result) {
				res.send(defaultOK);
				return db.close();
			});
		});
	});
}

userbase.authenticate = function(req, res) {
	var uniqueId = req.body.uniqueId;
	var password = req.body.password;
	
	db.open(function(err, db) {
		var mainColl = db.collection(userbase.mainCollection());
		mainColl.findOne({uniqueId: uniqueId, password: password}, function(err, result) {
			console.log(result);
			if (!result) {
				db.close();
				return res.send(wrongAuth);
			}
			db.close();
			return res.send(result);
		});
	});
}

userbase.init = function(app) {
	app.post("/userbase", this.insertUser);
	app.get("/userbase", this.authenticate);
}

